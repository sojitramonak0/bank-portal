/** @type {import('tailwindcss').Config} */
export default {
  content: ["./index.html", "./src/**/*.{js,ts,jsx,tsx}"],
  theme: {
    extend: {
      colors: {
       bgColor: '#F5F7FA',
       primary: '#D6D2D2',
       heading: '#343C6A',
       bold: '#232323',
       customGrey: '#718EBF',
       customLabel: '#B1B1B1'
      }
    },
  },
  plugins: [],
};

