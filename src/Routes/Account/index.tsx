import AccountDetails from "./Component/AccountDetails";
import CreditDebitOverView from "./Component/CreditDebitOverView";
import Invoice from "./Component/Invoice";
import LastTransaction from "./Component/LastTransaction";
import MyCardAccount from "./Component/MyCard";

const Account = () => {
  return (
    <div>
      <div>
        <AccountDetails />
      </div>
      <div className="flex">
        <LastTransaction />
        <MyCardAccount />
      </div>
      <div className="flex gap-7">
        <CreditDebitOverView />
        <Invoice />
      </div>
    </div>
  );
};

export default Account;
