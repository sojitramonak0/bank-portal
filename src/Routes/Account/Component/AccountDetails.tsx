import CommonBlock from "../../../Component/Common";
import { AccountDetailsList } from "../../../Constant";

const AccountDetails = () => {
  return (
    <div className="flex pl-10 gap-10 p-5 ">
      {AccountDetailsList.map((item) => {
        return (
          <div className="w-64">
          <CommonBlock
            image={item.image}
            label={item.label}
            price={item.price}
          />
          </div>
        );
      })}
    </div>
  );
};

export default AccountDetails;
