import { LastTransactionList } from "../../../Constant";

const LastTransaction = () => {
  return (
    <div className="p-3 pl-7">
      <div>
        <span className="text-heading font-semibold text-xl pl-2">
          Last Transaction
        </span>
      </div>
      <div className=" mt-5  bg-white rounded-2xl w-[750px] p-2">
        {LastTransactionList.map((item) => {
          return (
            <>
              <div className="gap-12 mb-3 mt-3 grid grid-cols-7">
                <div key={item.id} className="content-center pl-3">
                  <img src={item.image} className="size-12" alt="" />
                </div>
                <div className="content-center col-span-2 ">
                  <span className="block text-bold text-base font-semibold">
                    {item.label}
                  </span>
                  <span className="text-customGrey">{item.date}</span>
                </div>
                <div className="content-center ">
                  <span className="text-customGrey text-base font-normal">
                    {item.type}
                  </span>
                </div>
                <div className="content-center ">
                  <span className="text-customGrey text-base font-normal">
                    {item.cardNumber}
                  </span>
                </div>
                <div className="content-center ">
                  <span className="text-customGrey text-base font-normal">
                    {item.status}
                  </span>
                </div>
                <div className="content-center ">
                  <span className="text-green-400">{item.amount}</span>
                </div>
              </div>
            </>
          );
        })}
      </div>
    </div>
  );
};

export default LastTransaction;
