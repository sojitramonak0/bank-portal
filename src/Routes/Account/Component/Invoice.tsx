import { invoceList } from "../../../Constant";

const Invoice = () => {
  return (
    <div className="w-full p-2 pr-7">
      <div className="mb-5">
        <span className="text-heading font-semibold text-2xl">
          Invoice Sent
        </span>
      </div>
      <div className="bg-white  p-4  rounded-2xl ">
        {invoceList.map((item) => {
          return (
            
              <div className=" p-3 grid grid-cols-4">
                <div>
                  <img src={item.image} alt="" />
                </div>
                <div className="content-center col-span-2 ">
                  <span className="block text-customLabel text-base">{item.label}</span>
                  <span className="text-[#718EBF] text-normal">{item.time}</span>
                </div>
                <div className="flex justify-end p-4 ">
                  <span className="font-normal text-base">${item.amount}</span>
                </div>
            </div>
          );
        })}
      </div>
    </div>
  );
};

export default Invoice;
