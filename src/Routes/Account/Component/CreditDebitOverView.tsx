import Chart from "react-apexcharts";

const CreditDebitOverView = () => {
  const options = {
    highlightOnHover: false,
    series: [
      { data: [44, 55, 41, 64, 22, 43, 21] },
      { data: [53, 32, 33, 52, 13, 44, 32] },
    ],
    chart: {
      toolbar: {
        show: false,
      },
      type: "bar",
      height: 350,
    },
    states: {
      hover: {
        filter: {
          type: "none",
        },
      },
    },
    legend: {
      show: false,
    },
    stroke: {
      show: true,
      width: 11,
      colors: ["transparent"],
    },
    plotOptions: {
      bar: {
        borderRadius: 10,
        horizontal: false,
      },
    },
    colors: ["#1A16F3", "#FCAA0B"],
    tooltip: {
      enabled: false,
    },
    dataLabels: {
      enabled: false,
    },
    xaxis: {
      line: {
        show: false,
      },
      categories: ["Sun", "Mon", "Tue", "Wed", "Thu", "Fri", "Sat"],
      axisTicks: {
        show: false,
      },
      axisBorder: {
        show: false,
      },
      crosshairs: {
        show: true,
      },
    },
    grid: {
      yaxis: {
        lines: {
          show: false,
        },
      },
    },
    yaxis: {
      show: false,
    },
  };
  return (
    <div className="p-2 pl-7">
      <div className="">
        <span className="mb-5 pl-2 flex text-heading font-semibold text-2xl">
          Credit & Debit OverView
        </span>
        <div className="bg-white rounded-3xl w-[750px] ">
          <div className="pr-14 justify-between flex">
            <div className="flex mt-5 pl-5">
              $5,500 Credited & $2,570 Debited in this week
            </div>
            <div className="flex mt-5 gap-3">
              <div className="bg-[#1A16F3] w-4 h-4 mt-1 flex rounded-full"></div>
              <span>Credit</span>
              <div className="bg-[#FCAA0B] w-4 h-4 mt-1 rounded-full"></div>
              <span>Debit</span>
            </div>
          </div>
          <div className="flex justify-center">
            <Chart
              options={options}
              series={options.series}
              type="bar"
              height={300}
              width={700}
            />
          </div>
        </div>
      </div>
    </div>
  );
};

export default CreditDebitOverView;
