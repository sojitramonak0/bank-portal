import CreditCard from "../../../Component/CreditCard.tsx/GrediantCard";

const MyCardAccount = () => {
  return (
    <div className="p-3">
      <div className="">
        <div className="flex text-heading justify-between w-full">
          <span className="text-xl pl-3 font-semibold">My Cards</span>
          <a href="#" className="text-xl text-heading pr-6 font-semibold">
            See All
          </a>
        </div>
        <div className="flex p-4 justify-between">
          <div>
            <CreditCard />
          </div>
        </div>
      </div>
    </div>
  );
};

export default MyCardAccount;
