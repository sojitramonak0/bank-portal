import MyCard from "./Component/MyCard";
import QuickTransfer from "./Component/QuickTransfer";
import WeeklyActivity from "./Component/WeeklyActivity";

const Dashboard = () => {
  return (
    <>
      <div className="pl-4">
        <MyCard />
      </div>
      <div>
        <WeeklyActivity />
      </div>
      <div className="pl-9">
        <QuickTransfer />
      </div>
    </>
  );
};

export default Dashboard;
