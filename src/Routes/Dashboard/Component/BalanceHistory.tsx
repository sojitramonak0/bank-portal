import Chart from "react-apexcharts";

const BalanceHistory = () => {
  const series = [
    {
      name: "Series 1",
      data: [200, 500, 300, 750, 500, 650, 300],
    },
  ];

  const options = {
    chart: {
      height: 350,
      type: "area",
      toolbar: {
        show: false,
      },
    },
    dataLabels: {
      enabled: false,
    },
    stroke: {
      curve: "smooth",
    },
    grid: {
      show: true,
      strokeDashArray: 7,
      position: "back",
      xaxis: {
        lines: {
          show: true,
        },
      },
      yaxis: {
        lines: {
          show: true,
        },
      },
    },
    xaxis: {
      categories: ["Jul", "Aug", "Sep", "Oct", "Nov", "Dec", "Jan"],
      axisTicks: {
        show: true,
        borderType: "solid",
        color: "#78909C",
        width: 6,
      },
    },
    yaxis: {
      stepSize: 200,
      axisBorder: {
        show: false,
      },
      axisTicks: {
        show: true,
        borderType: "solid",
        color: "#78909C",
        width: 6,
      },
    },
    tooltip: {
      enabled: false,
    },
    legend: {
      enabled: false,
    },
  };
  return (
    <div className="mt-1.5">
      <div className="mb-3.5">
        <span className="text-heading font-semibold pl-5 text-2xl">
          Balance History
        </span>
      </div>
      <div className="bg-white rounded-3xl p-2">
        <Chart
          options={options}
          series={series}
          type="area"
          height={230}
          width={670}
        />
      </div>
    </div>
  );
};

export default BalanceHistory;
