import { Button } from "rsuite";
import BalanceHistory from "./BalanceHistory";

const QuickTransfer = () => {
  return (
    <div className="mt-3 gap-8 flex">
      <div className="w-2/5">
        <div>
          <span className="text-heading font-semibold pl-2 text-2xl">
            Quick Transfer
          </span>
        </div>
        <div className="mt-4 bg-white rounded-3xl">
          <div className="flex">
            <div className="p-8">
              <div className=" justify-center mt-.5 flex size-12">
                <img
                  src="https://i.pravatar.cc/150?u=1"
                  alt="profile"
                  className="rounded-full ml-8 w-20 "
                />
              </div>
              <span className="font-semibold text-base">Livia Bator</span>
              <span className="block justify-center font-semibold flex text-[#718EBF]">
                CEO
              </span>
            </div>
            <div className="p-8">
              <div className=" size-12">
                <img
                  src="https://i.pravatar.cc/150?u=1"
                  alt="profile"
                  className="rounded-full flex ml-5 "
                />
              </div>
              <span className=" text-base">Randy Press</span>
              <span className="block flex justify-center text-[#718EBF]">
                Director
              </span>
            </div>

            <div className="p-8">
              <div className=" size-12">
                <img
                  src="https://i.pravatar.cc/150?u=1"
                  alt="profile"
                  className="rounded-full ml-2"
                />
              </div>
              <span className=" text-base">Workman</span>
              <span className="block flex justify-center text-[#718EBF]">
                Designer
              </span>
            </div>
            <div className="flex ">
              <div className="content-center">
                <Button className="bg-white shadow-[#e0dce1] drop-shadow-2xl p-5 rounded-full">
                  <img src="src\assets\images\nextButton.png" alt="" />
                </Button>
              </div>
            </div>
          </div>
          <div>
            <div className="flex mt-4  pl-5 gap-10">
              <span className="text-[#718EBF] mt-3 text-base ">
                Write Amount
              </span>
              <div className=" rounded-3xl flex mb-10 bg-[#EDF1F7]">
                <input
                  type="text"
                  placeholder="258.25"
                  className="bg-[#EDF1F7] rounded-full pl-6 w-36 outline-none"
                />
                <button className="bg-[#1814F3] p-1 flex justify-center rounded-full gap-1 w-36">
                  <span className=" p-2 text-white font-medium text-base ">
                    send
                  </span>
                  <img
                    src="src\assets\images\sendIcon.png"
                    className="mt-2.5 pr-3 text-white  "
                    alt=""
                  />
                </button>
              </div>
            </div>
          </div>
        </div>
      </div>
      <div>
        <BalanceHistory />
      </div>
    </div>
  );
};

export default QuickTransfer;
