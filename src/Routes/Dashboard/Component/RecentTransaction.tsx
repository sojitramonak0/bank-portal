const RecentTransaction = () => {
  return (
    
      <div className="w-[370px] ">
        <div>
          <span className="text-2xl text-heading font-semibold p-2 flex">
            Recent Transaction
          </span>
        </div>
        <div className=" bg-white rounded-3xl mt-4 p-3">
          <div className="flex mb-4">
            <div>
              <img
                src="src\assets\images\Deposit (2).png"
                className="size-9 mt-1"
                alt=""
              />
            </div>
            <div className="pl-5">
              <span className="block text-base text-[#232323] font-medium">
                Deposit From My Card
              </span>
              <span className="font-normal text-sm text-[#718EBF]">
                28 january 2024
              </span>
            </div>
            <div className=" flex mt-2 pl-14">
              <span className="text-red-600"> -$8500 </span>
            </div>
          </div>
          <div className="flex mb-4">
            <div className="mt-1">
              <img
                src="src\assets\images\Paypal.png"
                className="size-9 mt-1"
                alt=""
              />
            </div>
            <div className="pl-5">
              <span className="block text-base text-[#232323]  font-medium">
                Deposit Paypal
              </span>
              <span className="font-normal text-sm text-[#718EBF]">
                28 january 2024
              </span>
            </div>
            <div className=" mt-2 flex justify-items-end pl-28">
              <span className="text-green-600 flex   "> +$55000 </span>
            </div>
          </div>
          <div className="flex mb-1">
            <div className="mt-1">
              <img
                src="src\assets\images\doller.png"
                className="size-9 mt-1"
                alt=""
              />
            </div>
            <div className="pl-5 ">
              <span className="block text-base text-[#232323] font-medium">
                jemima Rodrick
              </span>
              <span className="font-normal text-sm text-[#718EBF]">
                28 january 2024
              </span>
            </div>
            <div className=" mt-2 pl-[110px] ">
              <span className="text-green-600"> +$850 </span>
            </div>
          </div>
        </div>
      </div>
    
  );
};

export default RecentTransaction;
