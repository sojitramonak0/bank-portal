import CreditCard from "../../../Component/CreditCard.tsx/GrediantCard.tsx";
import SimpleCreditCard from "../../../Component/CreditCard.tsx/SimpleCard.tsx";
import RecentTransaction from "./RecentTransaction.tsx";

const MyCard = () => {
  return (
    <div className="flex gap-7">
      <div className="w-2/3 mt-4">
        <div className="flex text-heading justify-between p-2">
          <span className="text-xl pl-3 font-semibold">My Cards</span>  
          <a href="#" className="text-xl text-heading pl-3 font-semibold" >See All</a>
        </div>
        <div className="flex p-4 justify-between ">
          <div>
            <CreditCard />
          </div>
          <div>
            <SimpleCreditCard />
          </div>
        </div>
      </div>
      <div className="flex mt-4">
        <RecentTransaction />
      </div>
    </div>
  );
};

export default MyCard;
