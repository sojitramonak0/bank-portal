import Chart from "react-apexcharts";
import ExpenseStatistics from "./ExpenseStatistics";

const WeeklyActivity = () => {
  const options = {
    series: [
      { data: [44, 55, 41, 64, 22, 43, 21] },
      { data: [53, 32, 33, 52, 13, 44, 32] },
    ],
    chart: {
      type: "bar",
      toolbar: {
        show: false,
      },
    },

    plotOptions: {
      bar: {
        horizontal: false,
        borderRadius: 11,
      },
    },
    legend: {
      show: false,
    },
    stroke: {
      show: true,
      width: 11,
      colors: ["transparent"],
    },
    dataLabels: {
      enabled: false,
    },
    colors: ["#1814F3", "#16DBCC"],
    tooltip: {
      enabled: false,
      shared: false,
      intersect: false,
    },
    xaxis: {
      categories: ["Sun", "Mon", "Tue", "Wed", "Thu", "Fri", "Sat"],
    },
    yaxis: {
      axisTicks:{
        show: true,
        borderType: 'solid'
      },
    }
  };

  return (
    
      <div className="w-full flex">
        <div className="mt-2 w-2/3 pl-9">
          <span className="mb-5 pl-2 flex text-heading font-semibold text-2xl">
            Weekly Activity
          </span>
          <div className="bg-white rounded-3xl">
            <div className="justify-end pr-14 flex">
              <div className="flex mt-5 gap-3">
                <div className="bg-[#1814F3] w-4 h-4 mt-1 flex rounded-full"></div>
                <span>Deposit</span>
                <div className="bg-[#16DBCC] w-4 h-4 mt-1 rounded-full"></div>
                <span>Withdraw</span>
              </div>
            </div>
            <div className="flex justify-center">
              <Chart
                options={options}
                series={options.series}
                type="bar"
                height={300}
                width={730}
              />
            </div>
          </div>
        </div>
        <div >
          <ExpenseStatistics />
        </div>
      </div>
    
  );
};

export default WeeklyActivity;
