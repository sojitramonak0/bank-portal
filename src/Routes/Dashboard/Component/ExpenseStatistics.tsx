import Chart from "react-apexcharts";

const ExpenseStatistics = () => {
  const series = [30, 15, 35, 20];
  const options = {
    chart: {
      type: "pie",
    },
    labels: ["Entertainment", "Bill Expense", "Others", "investment"],
    theme: {
      monochrome: {
        enabled: true,
      },
    },
    fill: {
      colors: ["#343C6A", "#FC7900", "#1814F3", "#FA00FF"],
    },
    plotOptions: {
      pie: {
        startAngle: 10,
        endAngle: 360,
        dataLabels: {
          offset: -30,
        },
      },
    },
    stroke: {
      show: true,
      width: 5,
    },
    tooltip: {
      enabled: false,
    },
    dataLabels: {
      formatter(val, opts) {
        const name = opts.w.globals.labels[opts.seriesIndex];
        return [name, val.toFixed(1) + "%"];
      },
    },
    legend: {
      show: false,
    },
  };
  return (
    <div className="w-full h-[360px] pl-10">
      <span className="mb-5 pl-6 mt-2 flex text-heading font-semibold text-2xl">
        Expense Statistics
      </span>
      <div className="bg-white w-full h-full rounded-3xl">
        <Chart
          options={options}
          series={series}
          type="pie"
          height={380}
          width={360}
          
        />
      </div>
    </div>
  );
};

export default ExpenseStatistics;
