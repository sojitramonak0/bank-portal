import DetailsOfInvestment from "./Component/DetailsOfInvestment"
import MonthlyRevenue from "./Component/MonthlyRevenue"
import MyInvestment from "./Component/MyInvestment"
import TrendingStock from "./Component/TrendingStock"
import YearlyInvestment from "./Component/YearlyInvestment"

const Investments = () => {
  return (
    <div>
      <div><DetailsOfInvestment />  </div>
      <div className="flex">
        <YearlyInvestment />
        <MonthlyRevenue />
      </div>
      <div className="flex gap-5">
        <MyInvestment />
        <TrendingStock />
      </div>
    </div>
  )
}

export default Investments
