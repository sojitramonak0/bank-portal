import Chart from "react-apexcharts";

const MonthlyRevenue = () => {
    const options = {
        series: [
        {
          data: [1000, 25000, 15000, 50000, 20000, 40000]
        }
      ],
        chart: {
        height: 350,
        type: 'line',
        zoom: {
          enabled: false
        },
        toolbar: {
          show: false
        }
      },
      grid:{
        show: true,
        strokeDashArray: 5
      },
      colors: ['#16DBCC'],
      tooltip: {
        enabled: false,
      },
      stroke: {
        curve: 'smooth'
      },
      xaxis: {
        categories: ['2016', '2017', '2018', '2019', '2020', '2021'],
        axisBorder: {
          show: false,
        },
        axisTicks: {
          show: false,
        },
      },
      };
  return (
    <div className="p-6 ">
      <div>
        <span className="text-heading font-semibold text-2xl">Monthly Revenue</span>
      </div>
      <div  className="bg-white rounded-2xl mt-5" >
        <Chart
          options={options}
          series={options.series}
          height={300}
          width={580}
        />
      </div>
    </div>
  )
}

export default MonthlyRevenue
