import CommonBlock from "../../../Component/Common";
import { InvestmentList } from "../../../Constant";

function DetailsOfInvestment() {
  return (
    <div>
      <div className="flex gap-5 p-5 justify-between ">
        {InvestmentList.map((item) => {
          return (
            <div className="w-96">
              <CommonBlock
                image={item.image}
                label={item.label}
                price={item.amount}
                key={item.id}
              />
            </div>
          );
        })}
      </div>
    </div>
  );
}

export default DetailsOfInvestment;
