import Chart from "react-apexcharts";

const YearlyInvestment = () => {
    const options = {
        series: [
        {
          data: [1000, 25000, 15000, 50000, 20000, 40000]
        }
      ],
        chart: {
        type: 'line',
        zoom: {
          enabled: false
        },
        toolbar: {
          show: false
        }
      },
      grid:{
        show: true,
        strokeDashArray: 5
      },
      colors: ['#FCAA0B'],
      tooltip: {
        enabled: false,
      },
      
      markers: {
        size: 8
      },
      xaxis: {
        categories: ['2016', '2017', '2018', '2019', '2020', '2021'],
        axisBorder: {
          show: false,
        },
        axisTicks: {
          show: false,
        },
      },
      };
  return (
    <div className="p-6 w-[610px] ">
      <div>
        <span className="text-heading font-semibold text-2xl">Yearly Total Investment</span>
      </div>
      <div  className="bg-white rounded-2xl mt-5" >
        <Chart
          options={options}
          series={options.series}
          type="line"
          height={300}
          width={560}
        />
      </div>
    </div>
  );
};

export default YearlyInvestment;
