import { myInvestment } from "../../../Constant";

const MyInvestment = () => {
  return (
    <div className="w-2/4 p-3 pl-8">
      <div>
        <span className="text-heading font-semibold text-2xl">
          My Investment
        </span>
      </div>
      {myInvestment.map((item) => {
        return (
          <div className="flex bg-white mt-4 rounded-2xl justify-between p-3">
            <div className="pl-5 flex gap-4 ">
              <img src="src\assets\images\Apple.png" alt="" />
              <div className="content-center">
                <span className="block text-[#232323] font-semibold text-base ">
                  {item.label}
                </span>
                <span className="text-[#718EBF] text-sm font-normal">
                  {item.marketPlace}
                </span>
              </div>
            </div>

            <div className="content-center">
              <span className="block text-[#232323] font-semibold text-base">
                ${item.amount}
              </span>
              <span className="text-[#718EBF] text-sm font-normal">
                Investment value
              </span>
            </div>
            <div className="content-center">
              <span className="block text-[#232323] font-semibold text-base">
                {item.percentage}
              </span>
              <span className="text-[#718EBF] text-sm font-normal">
                Return value
              </span>
            </div>
          </div>
        );
      })}
    </div>
  );
};

export default MyInvestment;
