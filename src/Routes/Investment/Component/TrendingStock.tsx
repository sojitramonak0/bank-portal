import { Table } from "rsuite";

const { Column, HeaderCell, Cell } = Table;
const data = [
  { id: 1, sl_no: 1, name: "Apple", price: "$500", return: "15%" },
  { id: 2, sl_no: 2, name: "Tesla", price: "$5000", return: "20%" },
  { id: 3, sl_no: 3, name: "Bot", price: "$1000", return: "35%" },
  { id: 4, sl_no: 4, name: "LensCart", price: "$750", return: "22%" },
];

function TrendingStock() {
  return (
    <div className="p-3 w-[600px]">
      <div>
        <span className="text-heading text-2xl font-semibold">
          Trending Stock
        </span>
      </div>
      <div className="mt-4 p-4  bg-white rounded-3xl">
        <Table
          className="pl-5 font-normal text-base text-[#232323]"
          height={250}
          data={data}
          onRowClick={(rowData) => {
            console.log(rowData);
          }}
        >
          <Column width={100} align="center" fixed>
            <HeaderCell>SL No</HeaderCell>
            <Cell dataKey="sl_no" />
          </Column>

          <Column width={150}>
            <HeaderCell> Name</HeaderCell>
            <Cell dataKey="name" />
          </Column>

          <Column width={150}>
            <HeaderCell>Price</HeaderCell>
            <Cell dataKey="price" />
          </Column>

          <Column width={100}>
            <HeaderCell>Return</HeaderCell>
            <Cell dataKey="return" />
          </Column>
        </Table>
      </div>
    </div>
  );
}

export default TrendingStock;
