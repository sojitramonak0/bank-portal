import MyCard from "./Component/MyCard";
import RecentTransaction from "./Component/RecentTransaction";
// import "rsuite/dist/rsuite.css";

const Transactions = () => {
  return (
    <div>
      <div>
        <MyCard />
      </div>
      <RecentTransaction />
    </div>
  );
};

export default Transactions;
