import Chart from "react-apexcharts";

const MyExpense = () => {
  const options = {
    highlightOnHover: false,
    series: [
      {
        data: [30, 35, 15, 35, 56, 37],
      },
    ],
    chart: {
      toolbar: {
        show: false,
      },
      type: "bar",
      height: 350,
      // position: 'back',
    },
    states: {
      hover: {
        filter: {
          type: "none",
        },
      },
    },
    plotOptions: {
      bar: {
        borderRadius: 10,
        horizontal: false,
      },
    },
    colors: [
      function ({ value }) {
        if (value == 56) {
          return "#16DBCC";
        } else {
          return "#EDF0F7";
        }
      },
    ],
    tooltip: {
      enabled: false,
    },
    dataLabels: {
      enabled: false,
    },
    xaxis: {
      // position: 'back',
      line: {
        show: false,
      },
      categories: ["Aug", "Sep", "Oct", "Nov", "Dec", "Jan"],
      axisTicks: {
        show: false,
      },
      axisBorder: {
        show: false,
      },
      crosshairs: {
        show: true,
      },
    },
    grid: {
      yaxis: {
        lines: {
          show: false,
        },
      },
    },
    yaxis: {
      // position: 'back',
      show: false,
    },
  };

  return (
    <div className=" flex">
      <div className="mt-2 ">
        <span className="mb-5 pl-2 flex text-heading font-semibold text-2xl">
          My Expense
        </span>
        <div className="bg-white rounded-3xl ">
          <div className="flex justify-center">
            <Chart
              options={options}
              series={options.series}
              type="bar"
              height={200}
              width={380}
            />
          </div>
        </div>
      </div>
    </div>
  );
};

export default MyExpense;
