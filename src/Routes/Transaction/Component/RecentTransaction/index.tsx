import { Tabs } from "rsuite";
import AllTransaction from "./AllTransaction";
import { RecentTransactionData } from "../../../../Constant";

const RecentTransaction = () => {
  return (
    <div className="block">
       <div className=" p-3">
      <span className="font-semibold text-xl text-heading">Recent Transaction</span>
    </div>
      <Tabs defaultActiveKey="1" appearance="subtle">
        <Tabs.Tab eventKey="1"   title="All Transaction">
          <AllTransaction  data={RecentTransactionData}/>
        </Tabs.Tab>
        <Tabs.Tab eventKey="2" title="Income">
          <div>hii</div>
        </Tabs.Tab>
        <Tabs.Tab eventKey="3" title="Expense">
          <div>122</div>
        </Tabs.Tab>
      </Tabs>
    </div>
  );
};

export default RecentTransaction;
