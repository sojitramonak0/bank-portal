import { Button, Table } from "rsuite";
import ArrowUpwardIcon from "@mui/icons-material/ArrowUpward";
import Paginations from "../../../../Pagination";
import './RecentTransaction.css'

const { Column, HeaderCell, Cell } = Table;

const AllTransaction = ({ data }) => {
  return (
    <>
      <div className="w-full p-2 bg-white rounded-3xl mt-4">
        <Table
          width={1200}
          rowHeight={60}
          height={400}
          headerHeight={50}
          data={data}
          onRowClick={(data) => {
            console.log(data);
          }}
        >
          <Column width={200} colSpan={2}>
            <HeaderCell
              className="inline-block font-medium  text-base text-[#718EBF]"
              align="start"
            >
              Description
            </HeaderCell>
            <Cell>
              {(rowData) => {
                return (
                  <div className="gap-2 flex">
                    <button className="rounded-full  border-2 border-[#718EBF] bg-transparent ">
                      <ArrowUpwardIcon
                        className="text-[#718EBF]"
                        fontSize="medium"
                      />
                    </button>
                    <span className="inline-block font-normal text-base text-[#232323]">
                      {rowData.description}
                    </span>
                  </div>
                );
              }}
            </Cell>
          </Column>
          <Column width={170} colSpan={2}>
            <HeaderCell
              className="inline-block font-medium text-base text-[#718EBF]"
              align="start"
            >
              Transaction ID
            </HeaderCell>
            <Cell
              dataKey="Transaction_id"
              className="inline-block font-normal text-base text-[#232323]"
              align="start"
            />
          </Column>
          <Column width={170} colSpan={2}>
            <HeaderCell
              className="inline-block font-medium text-base text-[#718EBF]"
              align="start"
            >
              Type
            </HeaderCell>
            <Cell
              dataKey="Type"
              className="inline-block font-normal text-base text-[#232323]"
              align="start"
            />
          </Column>
          <Column width={170} colSpan={2}>
            <HeaderCell
              className="inline-block font-medium text-base text-[#718EBF]"
              align="start"
            >
              Card
            </HeaderCell>
            <Cell
              dataKey="Card"
              className="inline-block font-normal text-base text-[#232323]"
              align="start"
            />
          </Column>
          <Column width={170} colSpan={2}>
            <HeaderCell
              className="inline-block font-medium text-base text-[#718EBF]"
              align="start"
            >
              Date
            </HeaderCell>
            <Cell
              dataKey="Date"
              className="inline-block font-normal text-base text-[#232323]"
              align="start"
            />
          </Column>
          <Column width={140} colSpan={2}>
            <HeaderCell
              className="inline-block font-medium text-base text-[#718EBF]"
              align="start"
            >
              Amount
            </HeaderCell>
            <Cell
              dataKey="Amount"
              className="inline-block font-normal text-base text-[#232323]"
              align="start"
            />
          </Column>
          <Column width={170} colSpan={2}>
            <HeaderCell
              className="inline-block font-medium text-base text-[#718EBF]"
              align="start"
            >
              Receipt
            </HeaderCell>
            <Cell
              dataKey="Receipt"
              className="inline-block font-normal text-base text-[#232323]"
              align="start"
            >
              {(rowData) => (
                <Button
                className="download"
                  appearance="ghost"
                  onClick={() => alert(`id:${rowData.id}`)}
                >
                  Download
                </Button>
              )}
            </Cell>
          </Column>
        </Table>
      </div>
      <div className="p-5">
        <Paginations />
      </div>
    </>
  );
};

export default AllTransaction;
