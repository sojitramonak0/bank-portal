import CreditCard from "../../../Component/CreditCard.tsx/GrediantCard";
import SimpleCreditCard from "../../../Component/CreditCard.tsx/SimpleCard";
import MyExpense from "./MyExpense";

const MyCard = () => {
  return (
    <>
      <div className="flex gap-6">
        <div className="w-2/3 mt-4">
          <div className="flex text-heading justify-between p-2">
            <span className="text-xl pl-3 font-semibold">My Cards</span>
            <a href="#" className="text-xl pl-3 text-heading font-semibold">
              Add Card
            </a>
          </div>
          <div className="flex p-4 justify-between ">
            <div>
              <CreditCard />
            </div>
            <div>
              <SimpleCreditCard />
            </div>
          </div>
        </div>
        <div className="flex mt-4">
          <MyExpense />
        </div>
      </div>
    </>
  );
};

export default MyCard;
