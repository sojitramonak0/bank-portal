import { FaCreditCard, FaUser } from "react-icons/fa";
import {
  FaGear,
  FaHandHoldingDollar,
  FaMoneyBillTransfer,
} from "react-icons/fa6";
import { GoHomeFill } from "react-icons/go";
import { HiOutlineWrenchScrewdriver } from "react-icons/hi2";
import { IoIosBulb } from "react-icons/io";
import { IoStatsChartSharp } from "react-icons/io5";

const NavItemList = [
  {
    id: 1,
    eventKey: "Dashboard",
    icon: GoHomeFill,
    title: "Dashboard",
    href: "/",
  },
  {
    id: 2,
    eventKey: "Transactions",
    icon: FaMoneyBillTransfer,
    title: "Transactions",
    href: "/transaction",
  },
  {
    id: 3,
    eventKey: "Account",
    icon: FaUser,
    title: "Account",
    href: "/account",
  },
  {
    id: 4,
    eventKey: "Investments",
    icon: IoStatsChartSharp,
    title: "Investments",
    href: "/investments",
  },
  {
    id: 5,
    eventKey: "CreditCards",
    icon: FaCreditCard,
    title: "CreditCards",
    href: "/creditCard",
  },
  {
    id: 6,
    eventKey: "Loans",
    icon: FaHandHoldingDollar,
    title: "Loans",
    href: "/loans",
  },
  {
    id: 7,
    eventKey: "Services",
    icon: HiOutlineWrenchScrewdriver,
    title: "Services",
    href: "/services",
  },
  {
    id: 8,
    eventKey: "Privileges",
    icon: IoIosBulb,
    title: "Privileges",
    href: "/privileges",
  },
  {
    id: 9,
    eventKey: "Setting",
    icon: FaGear,
    title: "Setting",
    href: "/setting",
  },
];

const LastTransactionList = [
  {
    id: 1,
    image: "src\\assets\\images\\SpotifySubscription.png",
    label: "Spotify Subscription",
    date: "25 jan 2023",
    type: "Shopping",
    cardNumber: "1234***",
    status: "pending",
    amount: "$1000",
  },

  {
    id: 2,
    image: "src\\assets\\images\\mobileService (2).png",
    label: "Mobile Service",
    date: "30 feb 2023",
    type: "Service",
    cardNumber: "1234***",
    status: "Complete",
    amount: "$10000",
  },

  {
    id: 3,
    image: "src\\assets\\images\\moneyTransfer.png",
    label: "jemima rodrick",
    date: "15 june 2022",
    type: "Transfer",
    cardNumber: "1234***",
    status: "Complete",
    amount: "$20000",
  },
];

const AccountDetailsList = [
  {
    id: 1,
    label: "My Balance",
    price: "$12,000",
    image: "src\\assets\\images\\MyBalance.png",
  },
  {
    id: 2,
    label: "Income",
    price: "$5,600",
    image: "src\\assets\\images\\Income.png",
  },
  {
    id: 3,
    label: "Expense",
    price: "$3,600",
    image: "src\\assets\\images\\Expense.png",
  },
  {
    id: 4,
    label: "Total Saving",
    price: "$10,000",
    image: "src\\assets\\images\\TotalSaving.png",
  },
];

const RecentTransactionData = [
  {
    id: 1,
    description: "Spotify Subscription",
    Transaction_id: "#185234",
    Type: "Shopping",
    Card: 5451254,
    Date: "28 mar, 12:30 pm",
    Amount: 1000,
    Receipt: "hello",
  },
  {
    id: 2,
    description: "Freepik Sales",
    Transaction_id: "#1951357",
    Type: "Shopping",
    Card: 5451254,
    Date: "28 mar, 12:30 pm",
    Amount: 1000,
    Receipt: "hello",
  },
  {
    id: 3,
    description: "Mobile Service",
    Transaction_id: "#150314",
    Type: "Shopping",
    Card: 5451254,
    Date: "28 mar, 12:30 pm",
    Amount: 1000,
    Receipt: "hello",
  },
  {
    id: 4,
    description: "Spotify Subscription",
    Transaction_id: "#185234",
    Type: "Shopping",
    Card: 5451254,
    Date: "28 mar, 12:30 pm",
    Amount: 1000,
    Receipt: "hello",
  },
  {
    id: 5,
    description: "Freepik Sales",
    Transaction_id: "#1951357",
    Type: "Shopping",
    Card: 5451254,
    Date: "28 mar, 12:30 pm",
    Amount: 1000,
    Receipt: "hello",
  },
  {
    id: 6,
    description: "Spotify Subscription",
    Transaction_id: "#185234",
    Type: "Shopping",
    Card: 5451254,
    Date: "28 mar, 12:30 pm",
    Amount: 1000,
    Receipt: "hello",
  },
  {
    id: 7,
    description: "Freepik Sales",
    Transaction_id: "#1951357",
    Type: "Shopping",
    Card: 5451254,
    Date: "28 mar, 12:30 pm",
    Amount: 1000,
    Receipt: "hello",
  },
  {
    id: 1,
    description: "Spotify Subscription",
    Transaction_id: "#185234",
    Type: "Shopping",
    Card: 5451254,
    Date: "28 mar, 12:30 pm",
    Amount: 1000,
    Receipt: "hello",
  },
  {
    id: 2,
    description: "Freepik Sales",
    Transaction_id: "#1951357",
    Type: "Shopping",
    Card: 5451254,
    Date: "28 mar, 12:30 pm",
    Amount: 1000,
    Receipt: "hello",
  },
  {
    id: 3,
    description: "Mobile Service",
    Transaction_id: "#150314",
    Type: "Shopping",
    Card: 5451254,
    Date: "28 mar, 12:30 pm",
    Amount: 1000,
    Receipt: "hello",
  },
  {
    id: 4,
    description: "Spotify Subscription",
    Transaction_id: "#185234",
    Type: "Shopping",
    Card: 5451254,
    Date: "28 mar, 12:30 pm",
    Amount: 1000,
    Receipt: "hello",
  },
  {
    id: 5,
    description: "Freepik Sales",
    Transaction_id: "#1951357",
    Type: "Shopping",
    Card: 5451254,
    Date: "28 mar, 12:30 pm",
    Amount: 1000,
    Receipt: "hello",
  },
  {
    id: 6,
    description: "Spotify Subscription",
    Transaction_id: "#185234",
    Type: "Shopping",
    Card: 5451254,
    Date: "28 mar, 12:30 pm",
    Amount: 1000,
    Receipt: "hello",
  },
];
const invoceList = [
  {
    id: 1,
    image: "src\\assets\\images\\Apple store.png",
    label: "Apple Store",
    time: "5h ago",
    amount: "500",
  },
  {
    id: 2,
    image: "src\\assets\\images\\moneyTransfer.png",
    label: "Michel",
    time: "10h ago",
    amount: "1500",
  },
  {
    id: 3,
    image: "src\\assets\\images\\Paypal.png",
    label: "PayPal",
    time: "23h ago",
    amount: "580",
  },
  {
    id: 4,
    image: "src\\assets\\images\\moneyTransfer.png",
    label: "jemima Rodrick",
    time: "5min ago",
    amount: "5000",
  },
];

const InvestmentList = [
  {
    id: 1,
    image: "src\\assets\\images\\total investment.png",
    label: "Total invested Amount",
    amount: "$10000",
  },
  {
    id: 2,
    image: "src\\assets\\images\\no. of. Investment.png",
    label: "Number of Investment",
    amount: "1500",
  },
  {
    id: 3,
    image: "src\\assets\\images\\Rate of Return.png",
    label: "Rate of Return",
    amount: "+15%",
  },
];

const myInvestment = [
  {
    id: 1,
    image: "",
    label: "Apple Store",
    marketPlace: "E-commerce, MarketPlace",
    amount: "50,000",
    percentage: "15%",
  },
  {
    id: 2,
    image: "",
    label: "Samsung Mobile",
    marketPlace: "E-commerce, MarketPlace",
    amount: "10,000",
    percentage: "25%",
  },
  {
    id: 3,
    image: "",
    label: "Bot HeadPhone",
    marketPlace: "E-commerce, MarketPlace",
    amount: "8,500",
    percentage: "10%",
  },
  
];


export {
  NavItemList,
  LastTransactionList,
  AccountDetailsList,
  RecentTransactionData,
  invoceList,
  InvestmentList,
  myInvestment
};
