const CreditCard = () => {
  return (
    
      <div className=" text-white p-2 border-2 w-96 rounded-3xl bg-gradient-to-r from-[#4C49ED] to-[#0A06F4]">
        <div className="flex justify-between p-3 pl-5">
        <div >
          <span className="text-xs block">Balance</span>
          <span className="text-xl">$5,000</span>
        </div>
        <div>
            <img src="src\assets\images\Chip_Card.png" className="text-3xl pr-3"  alt='' /> 
        </div>
        </div>
        <div className="flex gap-9 pl-9 mt-5">
            <div >
                <span className="block font-lato text-xs text-primary font-normal">CARD HOLDER</span>
                <span className="text-base rs-text-semibold">Anddy Rubby</span>
            </div>
            <div>
                <span className="block text-xs text-primary font-normal">VALID THRU</span>
                <span className="text-base rs-text-semibold">12/22</span>
            </div>
        </div>
        <div className="flex mt-3 pl-5 justify-between  p-3 h-full ">
            <span className="mt-1">9652 **** **** 5214</span>
            <img src="src\assets\images\Group 17.png" className="pr-3" alt="" />
        </div>
      </div>
    
  );
};

export default CreditCard;
