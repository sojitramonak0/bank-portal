const SimpleCreditCard = () => {
  return (
    <div className="p-2 bg-white border w-96 rounded-3xl">
      <div className="flex justify-between pl-5 p-3">
        <div>
          <span className="text-xs block">Balance</span>
          <span className="text-xl">$5,000</span>
        </div>
        <div>
          <img
            src="src\assets\images\Chip_Card11.png"
            className="text-3xl pr-3"
            alt=""
          />
        </div>
      </div>
      <div className="flex gap-9 pl-9 mt-5">
        <div>
          <span className="block font-lato text-xs text-[#718EBF] font-normal">
            CARD HOLDER
          </span>
          <span className="text-base rs-text-semibold">Anddy Rubby</span>
        </div>
        <div>
          <span className="block text-xs text-[#718EBF] font-normal">
            VALID THRU
          </span>
          <span className="text-base rs-text-semibold">12/22</span>
        </div>
      </div>
      <div className="flex mt-3 justify-between pl-5 p-3 h-full ">
        <span className="mt-1">9652 **** **** 5214</span>
        <img src="src\assets\images\Group.png" className="pr-3" alt="" />
      </div>
    </div>
  );
};

export default SimpleCreditCard;
