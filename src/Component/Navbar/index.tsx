import { Nav, Sidenav } from "rsuite";
import { Icon } from "@rsuite/icons";
import { NavItemList } from "../../Constant";
import useNavbar from "./hooks/useNavbar";

const Navbar = () => {
 const {activeKey, handleSelect} = useNavbar()

  return (
    <div style={{ width: 250 }}>
      <div className="p-5 flex gap-3 ">
        <img src="src\assets\images\creditCard.png" alt="" />
        <p style={{ color: "#343C6A" }} className="  font-black  text-2xl">
          BankDash
        </p>
      </div>
      <div className="text-gray-500 w-56 flex flex-col p-2 justify-between font-sans ">
        <Sidenav>
          <Sidenav.Body>
            <Nav
              activeKey={activeKey}
              onSelect={handleSelect}
              className="block px-4 py-2 gap-2  "
            >
              {NavItemList.map((item) => {
                return (
                  <Nav.Item
                    key={item.id}
                    href={item.href}
                    eventKey={item.eventKey}
                    className={
                      "gap-3 flex mb-3 font-medium hover:bg-gray-100 rounded-lg p-2 btn" +
                      (activeKey === `${item.eventKey}` ? " text-blue-500" : "")
                    }
                  >
                    <Icon as={item.icon} className=" text-2xl " />
                    {item.title}
                  </Nav.Item>
                );
              })}
            </Nav>
          </Sidenav.Body>
        </Sidenav>
      </div>
    </div>
  );
};

export default Navbar;
