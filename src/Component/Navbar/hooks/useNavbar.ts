import { useEffect, useState } from "react";

const useNavbar = () => {
    const [activeKey, setActiveKey] = useState(() => {
        return localStorage.getItem("activeKey") || "Dashboard";
      });

      useEffect(() => {
        localStorage.setItem("activeKey", activeKey);
      }, [activeKey]);
      
      const handleSelect = (eventKey: string) => {
        setActiveKey(eventKey);
      };

      return{handleSelect, activeKey}
}

export default useNavbar