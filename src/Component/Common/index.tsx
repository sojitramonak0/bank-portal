interface AccountDetailProps{
    image:string,
    label:string,
    price:string
}

const CommonBlock = (props:AccountDetailProps) => {
    const {image, label, price} = props
  return (
    <div className="bg-white w-auto rounded-2xl flex">
      <div className="flex justify-center px-8">
        <img
          src={image}
          alt=""
          className="size-18 p-4"
        />
        <div className="content-center w-auto">
          <span className="block text-nowrap text-[#718EBF] font-normal text-base">{label}</span>
          <span className="text-[#232323] text-2xl  font-semibold">{price}</span>
        </div>  
      </div>
    </div>
  );
};

export default CommonBlock;
