import { Icon } from "@rsuite/icons";
import { FaSearch } from "react-icons/fa";

const Header = () => {
  return (
    <>
      <div className="flex p-5 gap-4 w-full h-20 justify-between">
        <div>
          <p className="font-semibold text-[#343C6A] text-2xl">Overview</p>
        </div>
        <div className="flex  gap-5 ">
          <div>
            <div className=" p-1  bg-slate-100 rounded-full">
              <Icon as={FaSearch} className=" p-1  text-[#718EBF] text-2xl" />
              <input
                type="text"
                placeholder="Search for something"
                className="pl-3 bg-slate-100 outline-none text-shadow-md text-sm"
              />
            </div>
          </div>
          <div className="flex   ">
            <img
              src="src\assets\images\Setting.png"
              className="size-9"
              alt=""
            />
          </div>
          <div className="flex ">
            <img
              src="src\assets\images\notification (2).png"
              className="size-9"
              alt=""
            />
          </div>
          <div className=" size-9">
            <img
              src="https://i.pravatar.cc/150?u=1"
              alt="profile"
              className="rounded-full"
            />
          </div>
        </div>
      </div>
    </>
  );
};

export default Header;
