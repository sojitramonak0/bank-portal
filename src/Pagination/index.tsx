import { useState } from "react";
import { Pagination } from "rsuite";

const Paginations = () => {
  const [activePage, setActivePage] = useState(1);

  return (
    <div className="flex justify-end">
       <Pagination
        prev
        last
        next
        first
        size="md"
        total={100}
        limit={30}
        activePage={activePage}
        onChangePage={setActivePage}
      />
    </div>
  );
};

export default Paginations;
