import { Pagination } from "rsuite";

const CustomPagination = ({totalPages, activePage, onSelect}) => {
  return (
    <div>
      <Pagination
        size="md"
        total={100}
        limit={20}
        activePage={activePage}
        pages={totalPages}
        onSelect={onSelect}
      />
    </div>
  );
};

export default CustomPagination;
