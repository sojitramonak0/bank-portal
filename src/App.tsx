import Navbar from "./Component/Navbar";
import "./App.css";
import Header from "./Component/Header";
import Dashboard from "./Routes/Dashboard";
import {
  BrowserRouter as Router,
  Route,
  Routes,
  Outlet,
} from "react-router-dom";
import Account from "./Routes/Account";
import CreditCard from "./Component/CreditCard.tsx/GrediantCard";
import Loans from "./Routes/Loans";
import Privileges from "./Routes/Privileges";
import Services from "./Routes/Services";
import Investments from "./Routes/Investment";
import Transactions from "./Routes/Transaction";
import Setting from "./Routes/Setting";

function App() {
  return (
    <div className="">
      <div className="flex ">
        <div className="sticky top-0 h-screen">
          <Navbar />
        </div>
        <div className="w-full">
          <div className="sticky top-0 bg-white">
            <Header />
          </div>
          <Outlet />
          <div className="bg-bgColor p-2 ">
          <Router>
            <Routes>
              <Route path="/" element={<Dashboard />} />
              <Route path="/transaction" element={<Transactions />} />
              <Route path="/investments" element={<Investments />} />
              <Route path="/account" element={<Account />} />
              <Route path="/creditCard" element={<CreditCard />} />
              <Route path="/loans" element={<Loans />} />
              <Route path="/privileges" element={<Privileges />} />
              <Route path="/services" element={<Services />} />
              <Route path="/setting" element={<Setting />} />
            </Routes>
          </Router>
          </div>
        </div>
      </div>
    </div>
  );
}

export default App;
